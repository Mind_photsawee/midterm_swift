//
//  ViewController.swift
//  selectedTest
//
//  Created by Admin on 5/3/2562 BE.
//  Copyright © 2562 KMUTNB. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var position: UITextField!
    @IBOutlet weak var product: UITextField!
    @IBOutlet weak var date: UIDatePicker!
    @IBOutlet weak var selectDate: UILabel!
    @IBAction func sendData(_ sender: Any) {
        let currentDate = date.date
        let myFormatter = DateFormatter()
        myFormatter.dateFormat = "dd/MM/YYYY"
        let currentDateText = myFormatter.string(from: currentDate)
        selectDate.text = currentDateText
           self.performSegue(withIdentifier: "InputVC", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "InputVC"){
            let displayVC = segue.destination as! ContentmentViewController
            displayVC.arr = [selectDate.text,product.text, position.text] as! [String]
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        selectDate.text = ""
        // Do any additional setup after loading the view, typically from a nib.
    }


}

