//
//  ContentmentViewController.swift
//  selectedTest
//
//  Created by Admin on 5/3/2562 BE.
//  Copyright © 2562 KMUTNB. All rights reserved.
//

import UIKit
import SQLite3

class ContentmentViewController: UIViewController {
    
    let fileName = "db2.sqlite"
    let fileManager = FileManager .default
    var dbPath = String()
    var sql = String()
    var db: OpaquePointer?
    var stmt: OpaquePointer?
    var pointer: OpaquePointer?
    var arr: [String] = []

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var contentmentText: UITextField!
    @IBAction func addContentment(_ sender: Any) {
        let content = contentmentText.text! as NSString
        
        self.sql = "insert into people values (null, '\(self.arr[0])', '\(self.arr[1])', '\(self.arr[2])', '\(content)')"
        print(self.sql)
        sqlite3_exec(self.db, self.sql, nil, nil, nil)
        self.select()
        
        print(content)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dbURL = try! fileManager .url(
            for: .documentDirectory,
            in: .userDomainMask,
            appropriateFor: nil,
            create: false)
            .appendingPathComponent(fileName)
        
        let opendb = sqlite3_open(dbURL.path, &db)
        if opendb != SQLITE_OK {
            print("Opening Database Error")
            return
        }
        else {
            print("Opening Database")
        }
        
        sql = "CREATE TABLE IF NOT EXISTS people " +
            "(id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "position TEXT, " +
            "product TEXT, " +
            "date TEXT, " +
            "content TEXT)"
        let createTb = sqlite3_exec(db, sql, nil, nil, nil)
        if createTb != SQLITE_OK {
            let err = String(cString: sqlite3_errmsg(db))
            print(err)
        }
        
        select()
        
        // Do any additional setup after loading the view.
    }
    
    func select() {
        sql = "SELECT * FROM people"
        sqlite3_prepare(db, sql, -1, &pointer, nil)
        textView.text = ""
        var id: Int32
        var content: String
        var position: String
        var product: String
        var date: String
        
        while(sqlite3_step(pointer) == SQLITE_ROW) {
            id = sqlite3_column_int(pointer, 0)
            textView.text?.append("id: \(id)\n")
            
            position = String(cString: sqlite3_column_text(pointer, 1))
            textView.text?.append("position: \(position)\n")
            
            date = String(cString: sqlite3_column_text(pointer, 2))
            textView.text?.append("date: \(date)\n")
            
            content = String(cString: sqlite3_column_text(pointer, 3))
            textView.text?.append("contents: \(content)\n")
            
            product = String(cString: sqlite3_column_text(pointer, 4))
            textView.text?.append("product: \(product)\n")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
